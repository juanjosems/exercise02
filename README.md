# Cloud Native Landing Challenge - Exercise 02

This project is an explanation about how i would able to deploy an instance of Jenkins using the documentation
obtained in [jenkins-helm](https://github.com/guipal/jenkins-helm).

### Installing 🔧

_How to install minikube in Ubuntu 20.04_

```
Steps to install minikube in the Linux computer
sudo apt update -y
sudo apt upgrade -y
sudo apt install -y curl wget apt-transport-https
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo cp minikube-linux-amd64 /usr/local/bin/minikube
sudo chmod +x /usr/local/bin/minikube
```

_How to check minikube is correctly installed_
```
minikube version

minikube version: v1.25.1
commit: 3e64b11ed75e56e4898ea85f96b2e4af0301f43d
```

_How to install kubectl utility_
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv kubectl /usr/local/bin/
kubectl version -o yaml
```

_How to check kubectl utility is correctly installed_
```
kubectl version -o yaml

clientVersion:
  buildDate: "2022-01-25T21:25:17Z"
  compiler: gc
  gitCommit: 816c97ab8cff8a1c72eccca1026f7820e93e0d25
  gitTreeState: clean
  gitVersion: v1.23.3
  goVersion: go1.17.6
  major: "1"
  minor: "23"
  platform: linux/amd64
serverVersion:
  buildDate: "2021-12-16T11:34:54Z"
  compiler: gc
  gitCommit: 86ec240af8cbd1b60bcc4c03c20da9b98005b92e
  gitTreeState: clean
  gitVersion: v1.23.1
  goVersion: go1.17.5
  major: "1"
  minor: "23"
  platform: linux/amd64
```

_How to init mikube using docker driver and the first error appeared_
```
minikube start --driver=docker 

* minikube v1.25.1 on Ubuntu 20.04
* Using the docker driver based on user configuration

* Exiting due to PROVIDER_DOCKER_NOT_FOUND: The 'docker' provider was not found: exec: "docker": executable file not found in $PATH
* Suggestion: Install Docker
* Documentation: https://minikube.sigs.k8s.io/docs/drivers/docker/
```

_Docker must be installed_
```
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

_How to check docker is installed and running_
```
docker -v

Docker version 20.10.12, build e91ed57
```

_I tried to run minikube and then another error appeared because i tried to start minikube with root user_
```
minikube start --driver=docker

Exiting due to DRV_AS_ROOT: The "docker" driver should not be used with root privileges.
```

_I tried this time with local no-admin user. The error was different but minikube still not start_
```
minikube start --driver=docker

X Exiting due to PROVIDER_DOCKER_NEWGRP: "docker version --format -" exit status 1: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get "http://%2Fvar%2Frun%2Fdocker.sock/v1.24/version": dial unix /var/run/docker.sock: connect: permission denied
* Suggestion: Add your user to the 'docker' group: 'sudo usermod -aG docker $USER && newgrp docker'
* Documentation: https://docs.docker.com/engine/install/linux-postinstall/
```

_How to add local user to docker users group_
```
sudo usermod -aG docker $USER && newgrp docker
```

_I tried to start again and this time minikube is working_
```
minikube start --driver=docker

* minikube v1.25.1 on Ubuntu 20.04
* Using the docker driver based on user configuration
* Starting control plane node minikube in cluster minikube
* Pulling base image ...
* Downloading Kubernetes v1.23.1 preload ...
    > gcr.io/k8s-minikube/kicbase: 378.98 MiB / 378.98 MiB  100.00% 5.13 MiB p/
    > preloaded-images-k8s-v16-v1...: 504.42 MiB / 504.42 MiB  100.00% 6.22 MiB
* Creating docker container (CPUs=2, Memory=2400MB) ...
* Preparing Kubernetes v1.23.1 on Docker 20.10.12 ...
  - kubelet.housekeeping-interval=5m
  - Generating certificates and keys ...
  - Booting up control plane ...
  - Configuring RBAC rules ...
* Verifying Kubernetes components...
  - Using image gcr.io/k8s-minikube/storage-provisioner:v5
* Enabled addons: storage-provisioner, default-storageclass
* Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

_Now i had another problem, because the exercise specifies that i need to install helm from the repo_
_But in the repo i can not find instructions about helm installation and i guessed that i need to install jenkins from_
_the repo. But i am not sure about that. To continue with the exercise i decide install helm_
 
```
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
```

_Then i obtain repo info as show in https://github.com/guipal/jenkins-helm_
```
helm repo add jenkins https://charts.jenkins.io
helm repo update
```

_How to install chart_
```
helm install jenkins-helm jenkins/jenkins --values jenkins-helm/values.yaml

NAME: jenkins-helm
LAST DEPLOYED: Wed Feb  2 20:48:46 2022
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get your 'admin' user password by running:
  kubectl exec --namespace default -it svc/jenkins-helm -c jenkins -- /bin/cat /run/secrets/chart-admin-password && echo
2. Get the Jenkins URL to visit by running these commands in the same shell:
  echo http://127.0.0.1:8080
  kubectl --namespace default port-forward svc/jenkins-helm 8080:8080

3. Login with the password from step 1 and the username: admin
4. Configure security realm and authorization strategy
5. Use Jenkins Configuration as Code by specifying configScripts in your values.yaml file, see documentation: http:///configuration-as-code and examples: https://github.com/jenkinsci/configuration-as-code-plugin/tree/master/demos

For more information on running Jenkins on Kubernetes, visit:
https://cloud.google.com/solutions/jenkins-on-container-engine

For more information about Jenkins Configuration as Code, visit:
https://jenkins.io/projects/jcasc/
```

_Jenkins has been installed but i can't access_
```
Trying port redirection
kubectl --namespace default port-forward svc/jenkins-helm 8080:8080

With curl i had a right response
curl -XGET http://127.0.0.1:8080
```

_I cloned the project in a temporal folder to read the values.yaml_
```
git clone https://github.com/guipal/jenkins-helm.git
```

_To work with minikube i need to set serviceType property value ClusterIP to NodePort_
```
serviceType: NodePort
```

_When i reinstalled helm jenkins i used the modified values.yaml_
```
helm uninstall jenkins-helm
helm install jenkins-helm jenkins/jenkins --values jenkins-helm/values.yaml

NAME: jenkins-helm
LAST DEPLOYED: Wed Feb  2 22:34:58 2022
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get your 'admin' user password by running:
  kubectl exec --namespace default -it svc/jenkins-helm -c jenkins -- /bin/cat /run/secrets/chart-admin-password && echo
2. Get the Jenkins URL to visit by running these commands in the same shell:
  export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services jenkins-helm)
  export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  echo http://$NODE_IP:$NODE_PORT/login

3. Login with the password from step 1 and the username: admin
4. Configure security realm and authorization strategy
5. Use Jenkins Configuration as Code by specifying configScripts in your values.yaml file, see documentation: http:///configuration-as-code and examples: https://github.com/jenkinsci/configuration-as-code-plugin/tree/master/demos

For more information on running Jenkins on Kubernetes, visit:
https://cloud.google.com/solutions/jenkins-on-container-engine

For more information about Jenkins Configuration as Code, visit:
https://jenkins.io/projects/jcasc/


NOTE: Consider using a custom image with pre-installed plugins
```

_I Had last error when i tried to login_
```
To Obtain admin password: 
kubectl exec --namespace default -it svc/jenkins-helm -c jenkins -- /bin/cat /run/secrets/chart-admin-password && echo

HTTP ERROR 403 No valid crumb was included in the request
URI:	/j_spring_security_check
STATUS:	403
MESSAGE:	No valid crumb was included in the request
SERVLET:	Stapler
Powered by Jetty:// 9.4.39.v20210325
```

_But i can access without user_
```
![Screenshot](https://bitbucket.org/juanjosems/exercise02/raw/main/02_jenkins_working.png)
![Screenshot](https://bitbucket.org/juanjosems/exercise02/raw/main/03_jenkins_working.png)
```


## Built with 🛠️

_There are the tools used to build the project_

* [Docker](https://docs.docker.com/)
* [Minikube](https://minikube.sigs.k8s.io/docs/)
* [Helm](https://helm.sh/)
* [Jenkins-helm](https://github.com/guipal/jenkins-helm)
* [gitignore file online generator](https://www.toptal.com/developers/gitignore)

## Decisions and security considerations 📖

### Decisions
- Although the exercise said i need to install helm from github i can't find the documentation so i guessed
 then the exercise refers to Jenkins instead of Helm. 

### Security considerations
- It is mandatory to fix the security configuring Security Realm and Authorization Strategy.
- You can use third parties software like LDAP as authorization strategy.
- You can use a certificate installed in server to ensure the communication.
- You can use secret Claims from HashiCorp Vault.

## Author ✒️
* **Juanjo Muñoz** - *Trabajo Inicial* - [juanjosems](https://juanjosems@bitbucket.org)

## License 📄

This project is free license

---
